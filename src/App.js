import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    // Open a connection
    var socket = new WebSocket('ws://rke-fuel-7000.herokuapp.com/');
    // var socket = new WebSocket('ws://localhost:7000/');

    // When a connection is made
    socket.onopen = function () {
      console.log('Opened connection ');

      // send data to the server
      var json = JSON.stringify({ message: 'Connection Established ' });
      socket.send(json);
    }

    // When data is received
    socket.onmessage = function (event) {
      // console.log(JSON.parse(event.data));
      // this.state.data.push(JSON.parse(event.data))
      var arr = []
      // console.log(JSON.parse(event.data))
      // var d = JSON.parse(event.data)
      arr.push(event.data)
      this.setState({ data: arr })
      // console.log(JSON.parse(this.state.data))
    }.bind(this)

    // A connection could not be made
    socket.onerror = function (event) {
      console.log(event);
    }

    // A connection was closed
    socket.onclose = function (code, reason) {
      console.log(code, reason);
    }

    // Close the connection when the window is closed
    window.addEventListener('beforeunload', function () {
      socket.close();
    });
  }


  render() {

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} alt="logo" />
          <p>{this.state.data.map((item, key) => {
            return item
          })}
          </p>
        </header>
      </div>
    );
  }
}

export default App;
